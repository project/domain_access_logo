<?php

namespace Drupal\domain_access_logo\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DomainAccess Logo SettingsForm.
 */
class DomainAccessLogoSettingsForm extends ConfigFormBase {

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Domain loader definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $domainLoader;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * Construct function.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheTagsInvalidatorInterface $cache_tags_invalidator, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->domainLoader = $entity_type_manager->getStorage('domain');
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * Create function return static entity type manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Load the ContainerInterface.
   *
   * @return \static
   *   return entity type manager configuration.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_tags.invalidator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_logo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['domain_access_logo.settings'];
  }

  /**
   * Function for config domain logo form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domains = $this->domainLoader->loadMultiple();

    if ($domains) {
      $form['general'] = [
        '#type' => 'details',
        '#title' => $this->t('Domain Logo Settings'),
        '#open' => TRUE,
      ];

      $domain_access_logo_config = $this->config('domain_access_logo.settings');

      // List of Image Extensions.
      $extensions = 'png gif jpg jpeg svg';

      foreach ($domains as $domain) {
        if ($domain->id()) {
          $form['general'][$domain->id()] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Upload logo for Domain: @hostname', ['@hostname' => $domain->label()]),
            '#description' => $this->t('The logo for this domain/subdomain.<br>Allowed types: @extensions.', [
              '@extensions' => $extensions,
            ]),
            '#size' => 64,
            '#default_value' => $domain_access_logo_config->get('logos.' . $domain->id()),
            '#upload_validators' => [
              'file_validate_extensions' => [$extensions],
            ],
            '#upload_location' => 'public://files',
            '#weight' => '0',
          ];
        }
      }
      return parent::buildForm($form, $form_state);
    }
    else {
      $url = Url::fromRoute('domain.admin');
      $domain_link = Link::fromTextAndUrl($this->t('Domain records'), $url)->toString();
      $form['title']['#markup'] = $this->t('There is no Domain record yet. Please create a domain records. See link: @domain_list', ['@domain_list' => $domain_link]);
      return $form;
    }
  }

  /**
   * Domain logo config form submit.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('domain_access_logo.settings');
    $domains = $this->domainLoader->loadOptionsList();
    foreach ($domains as $key => $value) {
      $previous_file_id = $config->get('logos.' . $key);
      $config->set('logos.' . $key, $form_state->getValue($key))->save();
      $image = $form_state->getValue($key);
      if (isset($image[0])) {
        $file = $this->fileStorage->load($image[0]);
        if ($file instanceof FileInterface) {
          if (isset($previous_file_id[0])) {
            if ((int) $file->id() !== $previous_file_id[0]) {
              $this->deletePreviousFile($previous_file_id[0]);
            }
          }
          $file->setPermanent();
          $file->save();
        }
      }
      elseif (isset($previous_file_id[0])) {
        $this->deletePreviousFile($previous_file_id[0]);
      }
    }
    $this->cacheTagsInvalidator->invalidateTags($this->configFactory->get('system.site')->getCacheTags());

    parent::submitForm($form, $form_state);
  }

  /**
   * Delete the previous file.
   *
   * @param int $id
   *   The ID from the file to delete.
   */
  protected function deletePreviousFile(int $id): void {
    $previous_file = $this->fileStorage->load($id);
    if ($previous_file instanceof FileInterface) {
      try {
        $previous_file->delete();
      }
      catch (EntityStorageException $exception) {
        $this->logger('domain_access_logo')->error($exception->getMessage());
      }
    }
  }

}
