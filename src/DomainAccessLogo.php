<?php

namespace Drupal\domain_access_logo;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\domain\DomainInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\file\FileInterface;

/**
 * Handy methods for the Domain Access Logo.
 */
class DomainAccessLogo {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructs a DomainAccessLogo object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DomainNegotiatorInterface $domain_negotiator, FileUrlGeneratorInterface $file_url_generator, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('domain_access_logo.settings');
    $this->domainNegotiator = $domain_negotiator;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * Return the active domain logo.
   *
   * @return string
   *   The patch to the logo.
   */
  public function getActiveDomainLogo(): string {

    // Check if we have an active domain.
    $current_domain = $this->domainNegotiator->getActiveDomain();
    if (!$current_domain instanceof DomainInterface) {
      return '';
    }

    // Check if there is a logo configured for the active domain.
    $logo = $this->config->get('logos.' . $current_domain->id());
    if (!isset($logo[0])) {
      return '';
    }

    // Check if we have an actual file.
    $file = $this->fileStorage->load($logo[0]);
    if (!$file instanceof FileInterface) {
      return '';
    }

    return $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
  }

}
