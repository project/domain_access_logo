# Domain Access Logo

## Description

This module provides uploading different logos for (sub)domains created using
the Domain module.

## Requirements

- [Domain](https://www.drupal.org/project/domain)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configuration for uploading a logo for each (sub)domain:
`admin/config/domain/domain_logo`

This will update the logo for each (sub)domain created through the Domain
module.
